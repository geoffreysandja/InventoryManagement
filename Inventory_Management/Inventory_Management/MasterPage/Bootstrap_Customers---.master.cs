﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Bootstrap_Customers : System.Web.UI.MasterPage
{
    string ConnectionString = ConfigurationManager.ConnectionStrings["PointofSaleConstr"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Cookies["InventMgtCustomerCookies"] != null)
            {
                string cookiesValues;
                cookiesValues = Request.Cookies["InventMgtCustomerCookies"]["CustID"];
                lblusername.Text = Request.Cookies["InventMgtCustomerCookies"]["CustID"];
                lblusername2.Text = Request.Cookies["InventMgtCustomerCookies"]["CustID"]; 
            }
            else
            {
                Response.Redirect("~/LoginCustomer.aspx", true);
            }
        }

    }

    

    protected void LinklogOut_Click(object sender, EventArgs e)
    {
        //   Session.Clear();
        ////  Response.Redirect("Login.aspx");

        if (Request.Cookies["InventMgtCustomerCookies"] != null)
        {
            Response.Cookies["InventMgtCustomerCookies"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect("../LoginCustomer.aspx");  //to refresh the page
        }
    }
}
