﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Bootstrap.master" AutoEventWireup="true" CodeFile="SalesReport.aspx.cs" Inherits="Sales_SalesReport" %>
 
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="atk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../assets/scripts/PrintPosCopy.js" type="text/javascript"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="col-lg-12"> 
            <div class="well well-sm" >
                 <div class="col-lg-12"> Report |     
            
                       <asp:TextBox ID="txtsearch"   Width="200px"   
                                ToolTip="Search by : Invoice No,  put Types like:  order or sales, POS"   Placeholder="Search" runat="server" AutoPostBack="True" 
                                ontextchanged="txtsearch_TextChanged"></asp:TextBox>|

                                        <atk:CalendarExtender ID="CalendarExtender2" runat="server" Format="yyyy-MM-dd" TargetControlID="txtDateFrom" />
        <asp:TextBox ID="txtDateFrom" runat="server" ToolTip="Your Starting Date"   
        placeholder="Starting Date" AutoPostBack="True" 
        ontextchanged="txtDateFrom_TextChanged"></asp:TextBox>

        <atk:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" TargetControlID="txtDateTo" />
        <asp:TextBox ID="txtDateTo" runat="server"  ToolTip="End Date"  AutoPostBack="True"   
        placeholder="End Date" ontextchanged="txtDateTo_TextChanged"></asp:TextBox> 
     
                           | <input type="button" class="btn btn-success btn-xs" value="Print"  onclick="javascript:printDiv('wrapper')" />    
                </div><br />
                   <atk:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtsearch"
                    MinimumPrefixLength="1" EnableCaching="true"      CompletionSetCount="1" CompletionInterval="100" 
                    ServiceMethod="GetMDN" FirstRowSelected="True">
                    </atk:AutoCompleteExtender>   
                          
            </div>
    
        <div class="panel panel-default">       
      
                <div class="panel-body">
                           
                    
                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="420px">
                        <div id="wrapper">
                                                       <span style="text-align:center"> 
                                <asp:Label ID="lblshopTitle" Font-Size="23px" runat="server" Text=""  Font-Names="High Tower Text"></asp:Label> <br />
                                <asp:Label ID="lblshopAddress"  Font-Size="11px" runat="server" Text=""></asp:Label>  <br />
                                <asp:Label ID="lblwebAddress"  Font-Size="11px"  runat="server" Text=""></asp:Label> <br />

                                <asp:Label ID="Label16" Font-Size="11px" runat="server" Text="Phone:"></asp:Label> 
                                <asp:Label ID="lblPhone" Font-Size="11px" runat="server" Text=""></asp:Label>  <br />
                                  <asp:Label ID="lblDatetime" Font-Size="11px" runat="server" Text=""></asp:Label> 
                               </span>   
                         <asp:Label ID="lbtotalRow" runat="server" Text="------"></asp:Label> <br /> <hr /> 
                            <asp:GridView ID="grdviewSalesReport" runat="server"   
                                class="table table-striped table-hover" Font-Size="11px" 
                                onrowdatabound="grdviewSalesReport_RowDataBound" ShowFooter="True">
                                                 
                                              
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </div>
         </div>
</div>

 

</asp:Content>




