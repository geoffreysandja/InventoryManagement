﻿<%@ Page Title="" Language="C#" MasterPageFile="../MasterPage/Bootstrap.master" AutoEventWireup="true" CodeFile="AddItem.aspx.cs" Inherits="AddItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="col-lg-8 ">
        <div class="well well-sm"  >
            <div class="col-lg-4" style="text-align:left">
                <asp:HyperLink ID="hlnkManageItems" Font-Size="20px" ForeColor="Black" ToolTip="Manage Items"  class="fa fa-th-list" NavigateUrl="~/Items/ManageItems.aspx"  ValidationGroup="vlpg11" runat="server"></asp:HyperLink>
                Manage Items 
              </div>
                <div class="col-lg-4" > 
                <asp:HyperLink ID="HyperLink2" Font-Size="20px" ForeColor="Black" ToolTip="Manage Items"  class="fa fa-cloud-upload" NavigateUrl="~/Items/UploadItems.aspx"  ValidationGroup="vlpg11" runat="server"></asp:HyperLink>
                Bulk Upload
            </div>
            <div class="col-lg-4" style="text-align:Right">
                <asp:HyperLink ID="hlnkAddCategory" Font-Size="20px" ForeColor="Black"  ToolTip="Add Category"  class="fa fa-plus" NavigateUrl="~/Items/Category.aspx"  ValidationGroup="vlpg11" runat="server"></asp:HyperLink>
                Add Category
            </div><br />
    </div>
 
       <div class="panel panel-primary" style="text-align:left">      
          <div class="panel-body">
                <asp:Label ID="Label1" class="label label-warning" Font-Size="12px" runat="server" Text="Add Item"></asp:Label>
                <asp:Label ID="Label8" runat="server" Font-Size="11px" Text="Please enter Item/product details below"></asp:Label> 
               
              <hr />
                        <div class="col-lg-6">
                                <asp:Label ID="Label2" runat="server" Font-Size="12px" Text="Item Code"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="vlpg43"   ForeColor="Red"  ControlToValidate="txtProductCode" runat="server"  ErrorMessage="*"></asp:RequiredFieldValidator>                   
                                <asp:TextBox ID="txtProductCode"  placeholder=" Item Code E.g: 8976732"  class="form-control"  ValidationGroup="vlpg43" runat="server"></asp:TextBox>

                                <asp:Label ID="Label3" runat="server" Font-Size="12px" Text="Item Name"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="vlpg43"   ForeColor="Red"  ControlToValidate="txtproductName" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator> 
                                Max 21 char
                                <asp:TextBox ID="txtproductName"   placeholder="Item Name "  class="form-control" ValidationGroup="vlpg43"  runat="server" MaxLength="21"></asp:TextBox>

                                <asp:Label ID="Label4" runat="server"  Font-Size="12px" Text="Purchase Price"></asp:Label> 
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="vlpg43"   ForeColor="Red"  ControlToValidate="txtpurchasePrice" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>    
                                <asp:TextBox ID="txtpurchasePrice"  placeholder="15"   class="form-control" ValidationGroup="vlpg43"  runat="server"></asp:TextBox>             

                                <asp:Label ID="Label6" runat="server"  Font-Size="12px" Text="Retail Price"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="vlpg43"   ForeColor="Red"  ControlToValidate="txtRetailPrice" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator> 
                                <asp:TextBox ID="txtRetailPrice"  placeholder="20"   class="form-control" ValidationGroup="vlpg43" runat="server"></asp:TextBox>


                        </div>
                        <div class="col-lg-6">

        <%--                
                            <asp:Label ID="Label9" runat="server"  Font-Size="12px" Text="Item Qutantity"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="vlpg43"   ForeColor="Red"  ControlToValidate="txtItemQty" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator> 
                            <asp:TextBox ID="txtItemQty"  placeholder="Qutantity e.g: 10 "   class="form-control" ToolTip="add Item Qutantity e.g: 10"  ValidationGroup="vlpg43" runat="server"></asp:TextBox>
--%>
                        
                            <asp:Label ID="Label5" runat="server"  Font-Size="12px" Text="Item Discount Rate"></asp:Label>  
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="vlpg43"   ForeColor="Red"  ControlToValidate="txtItemDiscRate" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator> 
                            <asp:TextBox ID="txtItemDiscRate" ToolTip="Disc Rate without % sign" placeholder="Disc Rate without % sign" class="form-control" ValidationGroup="vlpg43" Text="0.00"  runat="server"></asp:TextBox>


                            <asp:Label ID="Label7" runat="server"  Font-Size="12px" Text="Item Category"></asp:Label>  |  
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Items/Category.aspx" ToolTip="Please add Category" Font-Size="Larger">+</asp:HyperLink>                            
                            <asp:DropDownList ID="DDLCategory" class="form-control" ValidationGroup="vlpg43" runat="server"></asp:DropDownList>
                         
                            <asp:Label ID="Label9" runat="server"  Font-Size="12px" Text="Description"></asp:Label>  
                            <asp:TextBox ID="txtDescription" ToolTip="Description" 
                            placeholder="Description" class="form-control" ValidationGroup="vlpg43" 
                            Text=""  runat="server" Height="55px"></asp:TextBox>                         
                          <p></p>                            
                            
                            <asp:UpdatePanel ID="UpdatePanelImageUpload" runat="server"  UpdateMode="Conditional">
                                <ContentTemplate>                                
                                    <asp:FileUpload ID="FUpimg"     runat="server"    /> <br />
                                    <asp:Label ID="lblmessage" ForeColor="Red" runat="server" Font-Size="11px" Text=""></asp:Label> <p></p> 
                                    <asp:Button ID="btnSubmit" runat="server" ValidationGroup="vlpg43" class="btn btn-primary btn-sm" Text="Submit" onclick="btnSubmit_Click" />
                                </ContentTemplate>   
                                 <Triggers> <asp:PostBackTrigger   ControlID="btnSubmit"/></Triggers>      
                            </asp:UpdatePanel>                            
                             <br />

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ErrorMessage="Please add decimal value e.g: 20.11 or 20" ForeColor="Red"  ControlToValidate="txtpurchasePrice" ValidationGroup="vlpg43" 
                            ValidationExpression="^[0-9][\.\d]*(,\d+)?$"></asp:RegularExpressionValidator> <br />

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                            ErrorMessage="Please add decimal value" ForeColor="Red"  ControlToValidate="txtRetailPrice" ValidationGroup="vlpg43" 
                            ValidationExpression="^[0-9][\.\d]*(,\d+)?$"></asp:RegularExpressionValidator> <br />

                    <%--         <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                            ErrorMessage="Please add decimal value" ForeColor="Red"  ControlToValidate="txtItemQty" ValidationGroup="vlpg43" 
                            ValidationExpression="\d{0,9}"></asp:RegularExpressionValidator> <br />--%>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ErrorMessage="Please  add  Item Discount Rate" ForeColor="Red"  ControlToValidate="txtItemDiscRate" ValidationGroup="vlpg43" 
                            Display="Dynamic" ValidationExpression="^[0-9][\.\d]*(,\d+)?$"></asp:RegularExpressionValidator>
                        </div>
             </div>
        </div>
</div>
</asp:Content>

